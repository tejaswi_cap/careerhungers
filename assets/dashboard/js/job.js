var currentPath = $("#currentPath").val();
$(document).ready(function () {
    "use strict";

    //***** Post A Job Form Validation *****//
    $('#postAJobForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) { 
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = currentPath+'saveJob';
            params['successCallbackFunction'] = jobResponseData;
            doAjaxForm(params, "#postAJobForm");
        }
    });

    $(".select2").select2();

    $(document).ready(function() {
        // if ($("#mymce").length > 0) {
            tinymce.init({
                selector: "textarea#mymce",
                theme: "modern",
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker", "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking", "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
            });
        // }
    });

});

function jobResponseData(data) {
    if(data.response == "Success") {
        toastr["success"](data.message);
        /*window.location.href = currentPath+"screenerQuestions/"+data.job_id;*/
    } else {
        toastr["error"](data.message);
    }
}
