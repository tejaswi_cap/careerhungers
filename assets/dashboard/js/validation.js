$(document).ready(function () {
    "use strict";

    //***** Change Password Form Validation *****//
    $('#sampleForm').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var params = $.extend({}, doAjax_params_default);
            params['url'] = urlPath+'admin/profile/editname';
            params['successCallbackFunction'] = sampleData;
            doAjaxForm(params, "#changeNameForm");
        }
    });


});

function sampleData(data) {
    if(data.response == "1") {
        var mobile = $( "#changeMobileForm" ).find("#mobile").val();
        $(".mobileView").text(mobile);
        $("#changeMobileForm").find("#resendOtpCount").val(0);
        rightSlidebarAction("#changeMobileSideBar");
        $("#changeMobileForm").find("#otpDiv").hide();
        toastr["success"](data.message);
    } else {
        toastr["error"](data.message);
    }
}
