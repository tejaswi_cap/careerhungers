 

function loadMonthlyAppointmentChart(){

    var params = $.extend({}, doAjax_params_default);
    params['url'] = urlPath+"user/dashboard/monthlyappointments";
    params['data'] = {};
    params['successCallbackFunction'] = createMonthlyAppointmentChart;
    doAjax(params);

}


function createMonthlyAppointmentChart(data) {
    new Chartist.Line('#ct-visits', {
        labels: data.months,
        series: [
            data.inClinic,
            data.vc
        ]
    }, {
        top: 0,
        low: 1,
        showPoint: true,
        fullWidth: true,
        plugins: [
            Chartist.plugins.tooltip()
        ],
        axisY: {
            labelInterpolationFnc: function (value) {
                return (value / 1);
            }
        },
        showArea: true
    });
    
}


function loadWeeklyAppointmentChart(){

    var params = $.extend({}, doAjax_params_default);
    params['url'] = urlPath+"user/dashboard/weeklyappointments";
    params['data'] = {};
    params['successCallbackFunction'] = createWeeklyAppointmentChart;
    doAjax(params);

}


function createWeeklyAppointmentChart(data) {
    $("#weekAppointmentsCount").html(data.count);
    new Chartist.Bar('#ct-daily-sales', {
         labels: [],
         series: [
            data.values
        ]
    }, {
        axisX: {
            showLabel: false,
            showGrid: false,
            position: 'start'
        },
        chartPadding: {
            top: -20,
            left: 45,
        },
        axisY: {
            showLabel: false,
            showGrid: false,
            position: 'end'
        },
        height: 335,
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });
    
}


$(document).ready(function () {

    "use strict";

    loadMonthlyAppointmentChart();
    loadWeeklyAppointmentChart();

});


