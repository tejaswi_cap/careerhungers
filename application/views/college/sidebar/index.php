        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">CAP</span></h3> 
                </div>
                <ul class="nav" id="side-menu">
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="<?php echo site_url(); ?>images/profile/user.png" alt="user-img" class="img-circle"><span class="hide-menu"> <?php echo $this->session->userdata('adminName'); ?></span>
                        </a>
                    </li>
                    <li> 
                        <a href="<?php echo user_url; ?>" class="waves-effect"><i  class="mdi mdi-view-dashboard fa-fw"></i> <span class="hide-menu">Dashboard</span></a> 
                    </li>
                    <li class="devider"></li>
                    <li> 
                        <ul class="nav nav-second-level">
                            <li> 
                                <a href="<?php echo site_url(); ?>college/profile">
                                    <i class="mdi mdi-account-settings-variant fa-fw"></i>
                                    <span class="hide-menu">Profile Settings</span>
                                </a> 
                            </li>
                            <li> 
                                <a href="<?php echo site_url(); ?>college/course">
                                    <i class="mdi mdi-book-open fa-fw"></i>
                                    <span class="hide-menu">All Courses</span>
                                </a> 
                            </li>
                            <li> 
                                <a href="<?php echo site_url(); ?>college/faculty">
                                    <i class="mdi mdi-account-multiple fa-fw"></i>
                                    <span class="hide-menu">All faculty</span>
                                </a> 
                            </li>
                            <li> 
                                <a href="<?php echo site_url(); ?>college/placement">
                                    <i class="mdi mdi-briefcase-check fa-fw"></i>
                                    <span class="hide-menu">All placement</span>
                                </a> 
                            </li>
                        </ul>
                    </li>
                    <li class="devider"></li>
                    <li>
                        <a href="<?php echo site_url(); ?>admin/logout" class="waves-effect">
                            <i class="mdi mdi-logout fa-fw"></i> 
                            <span class="hide-menu">Log out</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>