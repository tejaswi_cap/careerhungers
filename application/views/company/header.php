<!DOCTYPE html>
<html lang="en">
<head>
   	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="<?php echo assets_path; ?>images/favicon.png"> -->
	<title>Dashboard Panel</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo dashboard_assets; ?>bootstrap/dist/css/bootstrap.min.css">
    <!-- Menu CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>theme/bower_components/sidebar-nav/dist/sidebar-nav.min.css">
    <!-- toast CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>theme/bower_components/toastr/build/toastr.css">
    <!-- Sweet Alert CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>theme/bower_components/sweetalert/sweetalert.css">
    <!-- morris CSS -->
    <link href="<?php echo site_url(); ?>theme/bower_components/morrisjs/morris.css" rel="stylesheet" type="text/css">
    <!-- chartist CSS -->
    <link href="<?php echo site_url(); ?>theme/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>theme/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?php echo site_url(); ?>theme/bower_components/datepicker/datepicker3.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- Calendar CSS -->
    <link href="<?php echo site_url(); ?>theme/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet"  type="text/css"/>
    <link href="<?php echo site_url(); ?>theme/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?php echo site_url(); ?>theme/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo site_url(); ?>theme/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <!-- Page plugins css -->
    <link href="<?php echo site_url(); ?>theme/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">

    <!-- Typehead CSS -->
    <link href="<?php echo site_url(); ?>theme/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">

    <!-- Select picker -->


    <link href="<?php echo site_url(); ?>theme/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo site_url(); ?>theme/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo site_url(); ?>theme/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo site_url(); ?>theme/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />


    <link href="<?php echo site_url(); ?>theme/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />


    <link rel="stylesheet" href="<?php echo site_url(); ?>theme/bower_components/dropify/dist/css/dropify.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>theme/bower_components/fancybox/ekko-lightbox.min.css" />
    <!-- animation CSS -->
    <link href="<?php echo dashboard_assets; ?>css/animate.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?php echo dashboard_assets; ?>css/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo dashboard_assets; ?>css/responsive.css" rel="stylesheet" type="text/css">
    <!-- color CSS -->
    <link href="<?php echo dashboard_assets; ?>css/colors/megna.css" id="theme" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
</head>
<body class="fix-header">
    <input type="hidden" id="urlPath" value="<?php echo site_url(); ?>">
	<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div id="wrapper">
        <?php 
            $this->load->view('navbar/index');
            $this->load->view('sidebar/index');
        ?>
    	<div id="page-wrapper">