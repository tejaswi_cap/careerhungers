            <footer class="footer text-center"> <?php echo date("Y"); ?> &copy; <a href="<?php echo site_url(); ?>"></a> All rights reserved</footer>
            <?php 
                $this->load->view("modal/index");
            ?>
        </div>
    </div>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo dashboard_assets; ?>bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo dashboard_assets; ?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <!-- <script src="<?php echo dashboard_assets; ?>js/waves.js"></script> -->
    <!--Counter js -->
    <script src="<?php echo site_url(); ?>theme/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/morrisjs/morris.js"></script>
    <!-- Animated skill bar -->
    <!-- <script src="<?php echo site_url(); ?>theme/bower_components/AnimatedSkillsDiagram/js/animated-bar.js"></script> -->
    <!-- chartist chart -->
    <script src="<?php echo site_url(); ?>theme/bower_components/chartist-js/dist/chartist.min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <?php if($this->router->fetch_class() == "calendar" && $this->router->fetch_method() == "index") { ?>
    <!-- Calendar JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/calendar/jquery-ui.min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/moment/moment.js"></script>
    <script src='<?php echo site_url(); ?>theme/bower_components/calendar/dist/fullcalendar.min.js'></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/calendar/dist/jquery.fullcalendar.js"></script>
    <?php } ?>
    <script src="<?php echo site_url(); ?>theme/bower_components/datatables/jquery.dataTables.min.js"></script>

    <script src="<?php echo site_url(); ?>theme/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>

    <script src="<?php echo site_url(); ?>theme/bower_components/Chart.js/Chart.min.js"></script>

    <script src="<?php echo site_url(); ?>theme/bower_components/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script src="<?php echo site_url(); ?>theme/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <!--Style Switcher -->

    
    <script src="<?php echo site_url(); ?>theme/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

    <!-- Bootstrap Select Picker-->


    <script src="<?php echo site_url(); ?>theme/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <script src="<?php echo site_url(); ?>theme/bower_components/multiselect/js/jquery.multi-select.js" type="text/javascript"></script>

    
    <!-- Clock Plugin JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>

    <!-- Toastr -->
    <script src="<?php echo site_url(); ?>theme/bower_components/toastr/toastr.js"></script>

    <!-- Tinymce Plugin Javascript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/tinymce/tinymce.min.js"></script>

    
    <!-- Sweet Alert -->
    <script src="<?php echo site_url(); ?>theme/bower_components/sweetalert/sweetalert.min.js"></script>
    <!-- Typehead Plugin JavaScript -->
    <script type="text/javascript" src="<?php echo site_url(); ?>theme/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>


    <!-- Dropzone Plugin JavaScript -->
    <script src="<?php echo site_url(); ?>theme/bower_components/dropzone-master/dist/dropzone.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>theme/bower_components/fancybox/ekko-lightbox.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo dashboard_assets; ?>js/custom.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/mask.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/validator.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/main.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/<?php echo $this->router->fetch_class(); ?>.js"></script>
    <script src="<?php echo dashboard_assets; ?>js/validation.js"></script>

    <!-- Custom tab JavaScript -->
    <script src="<?php echo dashboard_assets; ?>js/cbpFWTabs.js"></script>

    <script src="<?php echo site_url(); ?>theme/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script type="text/javascript">
            $('#myTable').DataTable();

    (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    })();
    </script>
        <?php if(isset($js_files)){ foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; }?>

</body>

</html>