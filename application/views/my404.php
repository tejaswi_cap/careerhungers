<style type="text/css">
.main{
  width : 65%;
  margin : 70px auto;
  text-align : center;
}
.main h1{
  font-size : 70px;
  font-weight : 400;
  color : #c80505;
}
.main h1 img{
  width : 11%;
}
.main h3{
  margin : 30px 0px;
  font-size : 28px;
  font-weight : 400;
  color : #000;
}
a.back{
  display : inline-block;
  color : #000;
  border : 1px solid #0067CC;
  padding : 7px 15px;
  margin-bottom : 30px;
}
</style>

<section class="notfound">
  <div class="container main text-center">
    <h1>404 PAGE NOT FOUND</h1>
    <h3>"Sorry! the page you are looking for can't be found"</h3>
    <a class="back" href="<?php echo site_url(); ?>">BACK TO HOME</a>
  </div>
</section>
  