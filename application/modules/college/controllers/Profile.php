<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/college/header';
    
    //set footer page
    public $footerPage = '../../views/college/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->model('Profile_model', 'my_model');
        $this->load->model('Brochure_model', 'brochure_model');
        $this->load->model('admin/location_model','location_model');
        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }

    public function index() {
        $id = $this->session->userdata('adminId');
        $data['college'] = $this->my_model->getCollegeDetails($id);
        $data['locations'] = $this->location_model->getAllLocations();
        $data['brochure'] = $this->brochure_model->getBrochures($id);
        $this->load->view($this->headerPage, $data);
        $this->load->view("profile/index", $data);
        $this->load->view($this->footerPage, $data);

    }

    public function editprofile() {

        $result = $this->my_model->editProfile();
        echo json_encode($result);

    }

    public function editprofileinfo() {

        $result = $this->my_model->editProfileInfo();
        echo json_encode($result);

    }

    public function editfacility() {

        $result = $this->my_model->editFacility();
        echo json_encode($result);

    }

    public function editbrochure() {

        $result = $this->my_model->editBrochure();
        echo json_encode($result);

    }
}
