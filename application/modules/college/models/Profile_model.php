<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    public function getCollegeDetails($id) {

        $query = "SELECT college.*,location.name as location FROM college 
                    JOIN `location` ON `location`.`id` = `college`.`location_id`
                    WHERE college.id = '".$id."'";
        $result = $this->db->query($query);
        $result = $result->row_array();
        return $result;

    }

    public function editProfile() {

        $response = array();
        extract($this->input->post());
        $id = $this->session->userdata('adminId');
        $query = "SELECT * FROM college WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        if($count != 0) {
            $data = array(
                "name" => $name,
                "location_id" => $location_id,
            );
            if(isset($sub_location)){
                $data['sub_location'] = $sub_location;
            }
            if(isset($admissions)){
                $data['admissions'] = $admissions;
            }
            if(isset($website)){
                $data['website'] = $website;
            }
            if(isset($year_of_establishment)){
                $data['year_of_establishment'] = $year_of_establishment;
            }
            if ($_FILES['logo']['name'] != '') {
                if (!file_exists('assets/college/logo')) {
                    mkdir('assets/college/logo', 0777, true);
                }
                $configProfile['upload_path'] = './assets/college/logo/'; 
                $configProfile['allowed_types'] = 'jpg|png|bmp|jpeg|';
                $configProfile['max_size']  = '0';
                $configProfile['max_width']  = '0';
                $configProfile['max_height']  = '0';
                $this->load->library('upload', $configProfile);
                $this->upload->initialize($configProfile);
                if ($this->upload->do_upload('logo')) {
                    // unlink($result->row_array()['logo']);
                    $imageData = $this->upload->data();
                    $data['logo'] = site_url()."assets/college/logo/".$imageData['file_name'];
                } else {
                    $data['message'] = $this->upload->display_errors();
                    $data['status'] = 0;
                }
            } else {
                $data['logo'] = '';
            }
            $this->db->where("id", $id);
            $result = $this->db->update("college", $data);
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Profile Updated Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        } else {
            $response = array(
                "response" => "Failure", 
                "message" => "Account not found"
            );
        }
        
        return $response;
    }
    
    public function editProfileInfo() {

        $response = array();
        extract($this->input->post());
        $id = $this->session->userdata('adminId');
        $query = "SELECT * FROM college WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        if($count != 0) {
            if(isset($type)){
                $data['type'] = $type;
            }
            if(isset($approved_by)){
                $data['approved_by'] = $approved_by;
            }
            if(isset($affiliated_to)){
                $data['affiliated_to'] = $affiliated_to;
            }
            if(isset($admission_criteria)){
                $data['admission_criteria'] = $admission_criteria;
            }
            $this->db->where("id", $id);
            $result = $this->db->update("college", $data);
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Profile Updated Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        } else {
            $response = array(
                "response" => "Failure", 
                "message" => "Account not found"
            );
        }
        
        return $response;
    }

    public function editFacility() {

        $response = array();
        extract($this->input->post());
        $id = $this->session->userdata('adminId');
        $query = "SELECT * FROM college WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        if($count != 0) {
            if(isset($library)){
                $data['library'] = $library;
            }
            if(isset($sports)){
                $data['sports'] = $sports;
            }
            if(isset($gym)){
                $data['gym'] = $gym;
            }
            if(isset($ac_campus)){
                $data['ac_campus'] = $ac_campus;
            }
            if(isset($computer_lab)){
                $data['computer_lab'] = $computer_lab;
            }
            $this->db->where("id", $id);
            $result = $this->db->update("college", $data);
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Profile Updated Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        } else {
            $response = array(
                "response" => "Failure", 
                "message" => "Account not found"
            );
        }
        
        return $response;
    }

    public function editBrochure() {

        $response = array();
        extract($this->input->post());
        $id = $this->session->userdata('adminId');
        $query = "SELECT * FROM college WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        if($count != 0) {
            if ($_FILES['file']['name'] != '') {
                if (!file_exists('assets/college/brochure')) {
                    mkdir('assets/college/brochure', 0777, true);
                }
                $configProfile['upload_path'] = './assets/college/brochure/'; 
                $configProfile['allowed_types'] = 'pdf|docx|doc|';
                $configProfile['max_size']  = '0';
                $configProfile['max_width']  = '0';
                $configProfile['max_height']  = '0';
                $this->load->library('upload', $configProfile);
                $this->upload->initialize($configProfile);
                if ($this->upload->do_upload('file')) {
                    // unlink($result->row_array()['brochure']);
                    $imageData = $this->upload->data();
                    $data['file'] = site_url()."assets/college/brochure/".$imageData['file_name'];
                } else {
                    $data['message'] = $this->upload->display_errors();
                    $data['status'] = 0;
                }
            } else {
                $data['file'] = '';
            }

            $data['college_id'] = $result->row_array()['id'];
            $this->db->set($data);
            $result = $this->db->insert("college_brochure");

            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Profile Updated Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        } else {
            $response = array(
                "response" => "Failure", 
                "message" => "Account not found"
            );
        }
        
        return $response;
    }

}