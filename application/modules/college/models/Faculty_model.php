<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faculty_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getCollegeFaculty($id) {

        $query = "SELECT a.*,b.name as qualification, c.name as course_name FROM college_faculty a
                    JOIN qualification b ON b.id = a.qualification_id
                    JOIN course_branch c ON c.id = a.course_id
                    WHERE a.college_id = '".$id."'";
        $result = $this->db->query($query);
        return $result->result();

    }

    public function getFacultyDetails($id) {

        $query = "SELECT a.*,b.name as qualification, c.name as course_name FROM college_faculty a
                    JOIN qualification b ON b.id = a.qualification_id
                    JOIN course_branch c ON c.id = a.course_id
                    WHERE a.id = '".$id."'";
        $result = $this->db->query($query);
        return $result->row_array();

    }

    public function editFaculty() {

        $response = array();
        extract($this->input->post());
        $college_id = $this->session->userdata('adminId');
        if($id == ''){
            $id = '';
        }
        $query = "SELECT * FROM college_faculty WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        $data = array(
            "college_id" => $college_id,
            "name" => $name,
            "qualification_id" => $qualification_id,
            "course_id" => $course_id,
            "designation" => $designation,
        );
        if(isset($experience)){
            $data['experience'] = $experience;
        }
        if(isset($description)){
            $data['description'] = $description;
        }
        if ($_FILES['image']['name'] != '') {
            if (!file_exists('assets/college/faculty')) {
                mkdir('assets/college/faculty', 0777, true);
            }
            $configProfile['upload_path'] = './assets/college/faculty/'; 
            $configProfile['allowed_types'] = 'gif|jpg|png|jpeg';
            $configProfile['max_size']  = '0';
            $configProfile['max_width']  = '0';
            $configProfile['max_height']  = '0';
            $this->load->library('upload', $configProfile);
            $this->upload->initialize($configProfile);
            if ($this->upload->do_upload('image')) {
                // unlink($result->row_array()['brochure']);
                $imageData = $this->upload->data();
                $data['image'] = site_url()."assets/college/faculty/".$imageData['file_name'];
            } else {
                $data['message'] = $this->upload->display_errors();
                $data['status'] = 0;
            }
        } else {
            $data['image'] = '';
        }

        if($count == 0 ) {
            $data['created_by'] = $college_id;
            $this->db->set($data);
            $result = $this->db->insert("college_faculty");
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Faculty added Successfully",
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong",
                );
            }

            
        } else {
            $data['updated_by'] = $college_id;
            $this->db->where("id", $id);
            $result = $this->db->update("college_faculty", $data);
                       
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Faculty Updated Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        }
        
        return $response;
    }

    public function deleteFaculty($id) {

        $response = array();
        
            $this->db->where("id", $id);
            $result = $this->db->delete("college_faculty");
                       
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Faculty Deleted Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        
        return $response;

    }


}