<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Faculty</h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active">Faculty</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="box-title m-b-30">Faculty</h3>
                    </div>
                    <div class="col-md-3 ">
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Qualification</th>
                                <th>Experience</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($faculty as $faculty){?>
                            <tr">
                                <td ><?php echo $faculty->name ?></td>
                                <td ><?php echo $faculty->designation ?> </td>
                                <td ><?php echo $faculty->course_name ?> </td>
                                <td ><?php echo $faculty->qualification ?></td>
                                <td ><?php echo $faculty->experience ?> years</td>
                                <td>
                                    <a href="<?php echo site_url('college/faculty/updatefaculty/'.$faculty->id); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-md m-r-5"><i class="ti-pencil-alt"></i></button></a>
                                    <button type="button" class="btn btn-info btn-outline btn-circle btn-md m-r-5 deleteFaculty" data-id="<?php echo $faculty->id ?>"><i class="icon-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel">
                <div class="panel-body">                                
                    <a href="<?php echo site_url(); ?>college/faculty/addfaculty" class="btn btn-info">Add faculty</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel wallet-widgets">
                <ul class="wallet-list">
                    <li><a href="<?php echo site_url(); ?>college/profile">Edit profile</a></li>
                    <li><a href="<?php echo site_url(); ?>college/course/addcourse">Add course</a></li>
                    <li><a href="<?php echo site_url(); ?>college/course">All courses</a></li>
                    <li><a href="<?php echo site_url(); ?>college/placement/addplacement">Add placement</a></li>
                    <li><a href="<?php echo site_url(); ?>college/placement">All placements</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>college/faculty/">
