<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Manage Candidates</h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active">Manage Candidates</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="box-title m-b-30">Manage Candidates</h3>
                    </div>
                    <div class="col-md-3 ">
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Applied on</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($applied_candidates as $candidate){?>
                            <tr>
                                <td class="manage-jobs-title"><a href="#"><?php echo $candidate->name ?></a></td>
                                <td class="table-date"><?php echo $candidate->location ?></td>
                                <td class="table-date"><?php echo date("d-m-Y", strtotime($candidate->created_at)) ?></td>
                                <td class="table-date"><?php echo $candidate->status ?></td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-rounded btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions<span><i class=" ti-angle-down right-side-toggle"></i></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo site_url('company/candidate/candidate/'.$candidate->id); ?>">View Candidate</a></li>
                                            <li><a href="#">Download resume</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel">
                <div class="panel-body">                                
                    <a href="<?php echo site_url(); ?>company/job/postJob" class="btn btn-info">Post a job</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel wallet-widgets">
                <ul class="wallet-list">
                    <li><a href="<?php echo site_url(); ?>company/job/postJob">Edit profile</a></li>
                    <li><a href="<?php echo site_url(); ?>company/job/postJob">Post a job</a></li>
                    <li><a href="<?php echo site_url(); ?>company/candidate">candidates</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>company/candidate/">
