<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Manage Candidates</h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active">Manage Candidates</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="panel">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h2><?php echo $candidate['name'] ?></h2>
                                <p><?php echo $candidate['email'] ?></p>
                                <p>Applied to <?php echo $candidate['title'] ?>, <?php echo $candidate['location'] ?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="m-t-50">
                                    <a href="#" class="btn btn-rounded btn-info btn-md m-r-10"><i class="fa fa-comment"></i></a>
                                    <a href="#" class="btn btn-rounded btn-info btn-md m-r-10"><i class="fa fa-phone"></i></a>
                                    <a href="#" class="btn btn-rounded btn-info btn-md m-r-10">Download resume</a><br>
                                </div>
                                <div class="m-t-20">
                                    <label for="status">Status:</label>
                                    <select id="status" class="status btn btn-rounded">
                                        <option>Awaiting Review</option>
                                        <option>Reviewed</option>
                                        <option>Contacting</option>
                                        <option>Rejected</option>
                                        <option>Hired</option>
                                    </select>
                                </div>
                                
                            </div>
                        </div>

                        <!-- Nav tabs -->
                        <ul class="nav customtab nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Application</span></a></li>
                            <li role="presentation" class=""><a href="#profile1" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Interview</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="home1">
                                <div class="col-md-12">
                                    <h3>Screener questions</h3>

                                    <div class="col-md-8">
                                        <h4>How many years of work experience do you have?</h4> 
                                        <p>You requirement:1 year</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p ><span class="text-success">2 years</span> </p>
                                    </div>
                                    <hr>
                                    <div class="col-md-8">
                                    <h4>What is the highest level of education you have completed?</h4> 
                                    <p>Your requirement: High scholl or equivalent</p>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="profile1">
                                <div class="col-md-12">
                                    <form action="#">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Date</label>
                                                    <div class="input-group">   
                                                        <input type="text" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                    </div>
                                                </div>                                                            
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Start time</label>
                                                        <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                                            <input type="text" class="form-control" value="13:14"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                                        </div>    
                                                    </div>                                                            
                                                </div>
                                                <div class="col-md-3 col-md-offset-1 col-xs-12">																	
                                                    <label class="text-center">to</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">End Time</label>
                                                        <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                                            <input type="text" class="form-control" value="13:14"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="control-label">Interview Type</label>
                                                    <div class="checkbox">
                                                        <input id="checkbox0" name="interview_type[]" type="checkbox">
                                                        <label for="checkbox0"> In person </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <input id="checkbox1" name="interview_type[]" type="checkbox">
                                                        <label for="checkbox1"> Phone </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <input id="checkbox2" name="interview_type[]" type="checkbox">
                                                        <label for="checkbox2"> Video </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Interview Address</label>
                                                    <input type="text" id="interview_address" name="interview_address" class="form-control" required > <span class="help-block hide"> This is inline help </span> </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Message</label>
                                                    <textarea class="form-control" name="message" placeholder="Include any additional information" rows="5"></textarea>
                                                    </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="m-t-20">
                                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                        </div>
                                        
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="panel wallet-widgets">
                <ul class="wallet-list">
                    <li><a href="<?php echo site_url(); ?>company/candidate">Candidates</a></li>
                    <li><a href="<?php echo site_url(); ?>company/job/postJob">Edit profile</a></li>
                    <li><a href="<?php echo site_url(); ?>company/job/postJob">Post a job</a></li>
                    <li><a href="<?php echo site_url(); ?>company/job">Posted jobs</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
<input type="hidden" id="currentPath" value="<?php echo site_url(); ?>company/candidate/">
