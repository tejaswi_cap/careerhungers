<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Job_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    public function getAllJobs() {

        $query = "SELECT * FROM jobs";
        $query = $this->db->query($query);
        return $query->result();

    }

    public function getjobDetails($id) {

        $query = "SELECT * FROM jobs WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $result = $result->row_array();
        return $result;

    }

    public function editJob() {

        $response = array();
        extract($this->input->post());
        $company_id = $this->session->userdata('adminId');
        if($id == ''){
            $id = '';
        }
        $query = "SELECT * FROM jobs WHERE id = '".$id."'";
        $result = $this->db->query($query);
        $count = $result->num_rows();
        $data = array(
            "company_id" => $company_id,
            "title" => $title,               
            "type" => $type,
            "qualification" => json_encode($qualification),
            "location" => json_encode($location),
            "description" => $description,
        );
        if(isset($branch)){
            $data['branch'] = json_encode($branch);
        }
        if(isset($min_salary)){
            $data['min_salary'] = $min_salary;
        }
        if(isset($max_salary)){
            $data['max_salary'] = $max_salary;
        }
        if(isset($hiring_process)){
            $data['hiring_process'] = json_encode($hiring_process);
        }
        if(isset($passout_year_from)){
            $data['passout_year_from'] = $passout_year_from;
        }
        if(isset($passout_year_to)){
            $data['passout_year_to'] = $passout_year_to;
        }
        if(isset($percentage_from)){
            $data['percentage_from'] = $percentage_from;
        }
        if(isset($cgpa_from)){
            $data['cgpa_from'] = $cgpa_from;
        }
        if(isset($min_salary)){
            $data['min_salary'] = $min_salary;
        }        
        if(isset($max_salary)){
            $data['max_salary'] = $max_salary;
        }
        if(isset($skills)){
            $data['skills'] = json_encode($skills);
        }
        if(isset($experience_from)){
            $data['experience_from'] = $experience_from;
        }
        if(isset($experience_to)){
            $data['experience_to'] = $experience_to;
        }
        if(isset($gender)){
            $data['gender'] = $gender;
        }
        if($count == 0 ) {
            $this->db->set($data);
            $result = $this->db->insert("jobs");
            $id =  $this->db->insert_id();
            foreach ($qualification as $quali){
                $job_quali = array(
                    'job_id' => $id,
                    'qualification_id' => $quali,
                );
                $this->db->set($job_quali);
                $this->db->insert("job_qualification");
            }
            foreach ($branch as $branch){
                $job_branch = array(
                    'job_id' => $id,
                    'course_branch_id' => $branch,
                );
                $this->db->set($job_branch);
                $this->db->insert("job_course_branch");
            }
            foreach ($location as $location){
                $job_location = array(
                    'job_id' => $id,
                    'location_id' => $location,
                );
                $this->db->set($job_location);
                $this->db->insert("job_location");
            }
            foreach ($hiring_process as $hiring_process){
                $job_hiring_process = array(
                    'job_id' => $id,
                    'hiring_process_id' => $hiring_process,
                );
                $this->db->set($job_hiring_process);
                $this->db->insert("job_hiring_process");
            }
            foreach ($skills as $skills){
                $job_skills = array(
                    'job_id' => $id,
                    'skills_id' => $skills,
                );
                $this->db->set($job_skills);
                $this->db->insert("job_skills");
            }
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Job posted Successfully",
                    "job_id" => $id, 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong",
                );
            }

            
        } else {
            $this->db->where("id", $id);
            $result = $this->db->update("jobs", $data);
            $this->db->delete('job_qualification', array('job_id' => $id));
            foreach ($qualification as $quali){
                $job_quali = array(
                    'job_id' => $id,
                    'qualification_id' => $quali,
                );
                $this->db->set($job_quali);
                $this->db->insert("job_qualification");
            }
            $this->db->delete('job_course_branch', array('job_id' => $id));
            foreach ($branch as $branch){
                $job_branch = array(
                    'job_id' => $id,
                    'course_branch_id' => $branch,
                );
                $this->db->set($job_branch);
                $this->db->insert("job_course_branch");
            }
            $this->db->delete('job_location', array('job_id' => $id));
            foreach ($location as $location){
                $job_location = array(
                    'job_id' => $id,
                    'location_id' => $location,
                );
                $this->db->set($job_location);
                $this->db->insert("job_location");
            }
            $this->db->delete('job_hiring_process', array('job_id' => $id));
            foreach ($hiring_process as $hiring_process){
                $job_hiring_process = array(
                    'job_id' => $id,
                    'hiring_process_id' => $hiring_process,
                );
                $this->db->set($job_hiring_process);
                $this->db->insert("job_hiring_process");
            }
            $this->db->delete('job_skills', array('job_id' => $id));
            foreach ($skills as $skills){
                $job_skills = array(
                    'job_id' => $id,
                    'skills_id' => $skills,
                );
                $this->db->set($job_skills);
                $this->db->insert("job_skills");
            }
                       
            if($result) {
                $response = array(
                    "response" => "Success", 
                    "message" => "Job Updated Successfully", 
                );
            } else {
                $response = array(
                    "response" => "Failure", 
                    "message" => "Something Went Wrong"
                );
            }
        }
        
        return $response;
    }


}