<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?= $title?></h4> 
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo user_url; ?>dashboard">Dashboard</a></li>
                <li class="active"><?= $title?></li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Blog-component -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="box-title m-b-30"><?= $title?></h3>
                    </div>
                    <div class="col-md-3 ">
                    </div>
                </div>
                <div >
				<?php echo $output; ?>
                </div>
            </div>
        </div>
    </div>

</div>
