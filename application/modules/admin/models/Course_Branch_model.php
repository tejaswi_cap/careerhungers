<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Course_Branch_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    // function getAllCourseBranches()
    // {
    //     $query = "SELECT * FROM course_branch WHERE is_active = 1 AND is_deleted = 0 ";
    //     $query = $this->db->query($query);
    //     return $query->result();
    // }

    function getAllCourseBranches()
    {
        $query = "SELECT qualification.name as qualification,course_branch.* 
                    FROM course_branch
                    JOIN qualification ON course_branch.qualification_id = qualification.id
                    WHERE course_branch.is_active = 1 AND course_branch.is_deleted = 0";
        $query = $this->db->query($query);
        return $query->result();
    }

    
}