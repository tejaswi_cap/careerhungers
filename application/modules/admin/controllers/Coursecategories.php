<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coursecategories extends CI_Controller {
    
    //set Header page
    public $headerPage = '../../views/admin/header';
    
    //set footer page
    public $footerPage = '../../views/admin/footer';
    
    public function __Construct() {

        parent::__Construct();
        $this->load->database();

        if ($this->session->userdata('adminId') == '') {
            redirect(base_url());
        }

    }


}
