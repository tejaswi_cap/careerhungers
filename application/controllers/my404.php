<?php if (! defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class My404 extends CI_Controller
{
	public function index()
	{
		$data = array();
		if(count($this->uri->segments) > 0)
		{
			$urls = $this->uri->segments;
			if(isset($urls[1])) $data['city'] = $urls[1];
			if(isset($urls[2])) $data['speciality'] = str_replace("-", " ", $urls[2]);
		}
		$this->load->view('my404',$data);
	}
}
