$(document ).ready(function() {
    $(document).on("click", '.status', function(event) {
        var id = $(this).data("value");
        var path = document.getElementById("statuspath"+id).value;  
        var status = document.getElementById("status"+id).value;  
        var statusRow = document.getElementById("statusRow"+id).value;  
        var table = document.getElementById("dbtable"+id).value;  
        var url = path+"/"+table+"/"+id+"/"+status+"/"+statusRow;


        swal({ title: "Do you want to Change the Status" ,
			showCancelButton: true,
			confirmButtonText: 'Yes',
			confirmButtonColor : "#3C8DBC",
			closeOnConfirm: false,
			allowOutsideClick: false,
			allowEscapeKey: false
        },
        function() {		
			swal.disableButtons();    
			$.ajax({
				type : 'POST',
				data : {'ids': id},
				url  : url,
				success: function(){
					swal.close();
					toastr["success"]("Status Changed Successfully");
                            if(status == 1) {
						        document.getElementById("status"+id).value = 0;
						        var content = '<span class="label label-danger arrowed"><a class="status" data-value="'+id+'" style="color:#FFF; text-decoration:none; cursor:pointer;">Deactivated</a></span>';
                            } else {
						        document.getElementById("status"+id).value = 1;
						        var content = '<span class="label label-success arrowed-in arrowed-in-right"><a class="status" data-value="'+id+'" style="color:#FFF; text-decoration:none; cursor:pointer;">Activated</a></span>';
                            } 
						$("#statusHtml"+id).html(content);
				}
			});
		});  
	});


	var myTable = $('#dataTable').DataTable();
	
	$(document).on("click", '.deleteRow', function(event) {
        var id = $(this).data("value");
       	var rowNumber = $(this).closest('tr').index();
        var path = document.getElementById("deletepath"+id).value;
        var deleteRow = document.getElementById("deleteRow"+id).value;    
        var table = document.getElementById("dbtable"+id).value;  
        var url = path+"/"+table+"/"+id+"/"+deleteRow;

        swal({ title: "Are you sure?" ,
			text: "You will not be able to recover this data!",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No',
			closeOnConfirm: false,
			confirmButtonColor : "#DD6B55",
			allowOutsideClick: false,
			allowEscapeKey: false
        },
        function() {		
			swal.disableButtons(); 
            window.onkeydown = null;
            window.onfocus = null; 
			$.ajax({
				type : 'POST',
				data : {'ids': id},
				url  : url,
				success: function(){
					
					swal.close();
					toastr["success"]("Deleted Successfully");
                        var site_url = $("#base_url").val();
                        var controllerName = $("#controllerName").val();
                        myTable.row($("#deleteRow"+id).parents('tr')).remove();
                        myTable.ajax.url(site_url+"dashboard/"+controllerName+"/gettablesdata").load();
                        $("form input.error, select.error").removeClass("error");
                        $("form input,form textarea").not("[type=submit],[type=hidden],[type=button]").val("");
				}
			});
		});  
	});


	$(document).on("click", '.deleteRowPatient', function(event) {
        var id = $(this).data("value");
        var patient = $(this).data("patient");
        
        var path = document.getElementById("deletepath"+patient).value;
        var deleteRow = document.getElementById("deleteRow"+patient).value;    
        var table = document.getElementById("dbtable"+patient).value;  
        var url = path+"/"+table+"/"+id+"/"+deleteRow;

        swal({ title: "Are you sure?" ,
			text: "You will not be able to recover this data!",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No',
			closeOnConfirm: false,
			confirmButtonColor : "#DD6B55",
			allowOutsideClick: false,
			allowEscapeKey: false
        },
        function() {		
			swal.disableButtons(); 
            window.onkeydown = null;
            window.onfocus = null;  
			$.ajax({
				type : 'POST',
				dataType: "json",
				data : {'ids': id},
				url  : url,
				success: function(data){
					swal.close();
					toastr["success"]("Deleted Successfully");
					var site_url = $("#base_url").val();
					var controllerName = $("#controllerName").val();
					myTable.row($("#deleteRow"+id).parents('tr')).remove();
					myTable.ajax.url(site_url+"dashboard/"+controllerName+"/gettablesdata").load();
					$("#addPatient input,#addPatient textarea").not('[type=submit],[type=hidden],[type=button]').val("").removeAttr("readonly");
	    			$('#addPatient #uniqueDisplayNumber,#addPatient #clinicNextNumber').val(data.patientNumber).removeAttr('disabled','');
					$("#InputId").val(0);
				}
			});
		});  
	});


    $(document).on("click", '.composeSms', function(event) {
        var url = $(this).data("url");
        swal({ 
        	html:true,
            title: "Disclamier" ,
            text: "<html><ul style='text-align:justify;'><li>24 Hours Required.</li><li>Since its promotional SMS, it will be sent between 10AM to 6Pm only.</li><li>You can send upto 1000 SMS per month.</li></ul><p>* We will display Gigadocs Link</p></html>",
            showCancelButton: true,
            confirmButtonText: 'Proceed',
            confirmButtonColor : "#3C8DBC",
            closeOnConfirm: false,
            allowOutsideClick: false,
            allowEscapeKey: false
        },
        function() {        
            swal.disableButtons();    
            window.location.href = url
        });  
    });
});